# SBP

[Sociedade Brasileira de Patologia](http://congressodepatologia.org.br/)

## Instalação:

```sh
$ git clone git@bitbucket.org:domini/sbp.git
$ npm install
$ bower install
$ grunt
```

## Startando o app via Katon em: [sbp.ka](http://sbp.ka/)

```sh
$ npm install -g katon
$ sudo katon install && katon start
$ katon add 'grunt'
```

## Assets

* SCSS: `app/scss`
	1. Assets SCSS do foundation em `app.scss`
	2. SCSS criado para o app em `appstyles.scss`